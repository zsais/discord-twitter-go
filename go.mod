module hello-run

go 1.13

require (
	//github.com/bwmarrin/discordgo v0.23.2
	github.com/hashicorp/go-retryablehttp v0.6.8
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
