package main

import (
	//"github.com/bwmarrin/discordgo"

	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

func main() {

	recentTweetID := getRecentTweetTimeline()

	recentTweet := getRecentTweet(recentTweetID)

	postTweetToChannel(recentTweet)

	// Define HTTP server.
	// http.HandleFunc("/", helloRunHandler)

	// PORT environment variable is provided by Cloud Run.
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Print("Hello from Cloud Run! The container started successfully and is listening for HTTP requests on $PORT")
	log.Printf("Listening on port %s", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func getRecentTweetTimeline() string {
	// TWITTER_ID environment variable is provided by Cloud Run.
	twitterID := os.Getenv("TWITTER_ID")

	// BEARER environment variable is provided by Cloud Run.
	bearerToken := os.Getenv("BEARER")

	url := "https://api.twitter.com/2/users/" + twitterID + "/tweets"

	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + bearerToken

	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)

	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}
	defer resp.Body.Close()

	var data map[string]interface{}
	err1 := json.NewDecoder(resp.Body).Decode(&data)
	if err1 != nil {
		log.Println("Error while reading the response bytes:", err1)
	}

	return data["meta"].(map[string]interface{})["newest_id"].(string)
}

func getRecentTweet(recentTweetID string) string {

	// BEARER environment variable is provided by Cloud Run.
	bearerToken := os.Getenv("BEARER")

	url := "https://api.twitter.com/2/tweets/" + recentTweetID

	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + bearerToken

	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)

	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}
	defer resp.Body.Close()

	var data map[string]interface{}
	err1 := json.NewDecoder(resp.Body).Decode(&data)
	if err1 != nil {
		log.Println("Error while reading the response bytes:", err1)
	}

	return data["data"].(map[string]interface{})["text"].(string)
}

func postTweetToChannel(recentTweet string) {

	// DISCORD_URL environment variable is provided by Cloud Run.
	url := os.Getenv("DISCORD_URL")
	log.Println(url)

	payload, _ := json.Marshal(map[string]string{"content": recentTweet})

	payloadBody := bytes.NewBuffer(payload)

	// Create a new request using http
	req, err := http.Post(url, "application/json", payloadBody)

	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}

	defer req.Body.Close()
}
